# syntax = docker/dockerfile:experimental
FROM debian:buster-slim AS deb-downloader
RUN apt-get update -q
RUN apt-get install --no-install-recommends -y apt-transport-https dirmngr gnupg ca-certificates
RUN apt-get update -q
RUN rm /etc/apt/apt.conf.d/docker-clean
RUN apt-get install --download-only -y --no-install-recommends \
  bison \
  build-essential \
  default-libmysqlclient-dev \
  libbz2-dev \
  libcurl4 \
  libcurl4-openssl-dev \
  libedit-dev \
  libffi-dev \
  libicu-dev \
  libjpeg-dev \
  libkrb5-dev \
  liblttng-ctl-dev \
  liblttng-ctl0 \
  liblzma-dev \
  libncurses-dev \
  libncurses5-dev \
  libncursesw5-dev \
  libonig-dev \
  libpng-dev \
  libpq-dev \
  libre2-dev \
  libreadline-dev \
  libsqlite3-dev \
  libssl-dev \
  libtool \
  libxml2-dev \
  libxmlsec1-dev \
  libxslt-dev \
  libxslt1-dev \
  libyaml-dev \
  libzip-dev \
  zlib1g \
  zlib1g-dev

FROM debian:buster-slim
ENV ASDF_DATA_DIR="/opt/asdf"
ENV PATH="${ASDF_DATA_DIR}/shims:${ASDF_DATA_DIR}/bin:/opt/gitlab/.local/bin:${PATH}"
ENV TERM="xterm"
WORKDIR /opt/gitlab
COPY config/01_nodoc /etc/dpkg/dpkg.cfg.d/01_nodoc
RUN mkdir -p /opt/toolcache/common
COPY --from=deb-downloader /var/cache/apt/archives/*.deb /opt/toolcache/common/
ADD https://rubygems.org/downloads/bundler-1.17.3.gem /opt/toolcache/
ADD https://rubygems.org/downloads/bundler-2.1.4.gem /opt/toolcache/
COPY pkg/asdf*.deb /opt/toolcache/
COPY pkg/dotnet*.deb /opt/toolcache/
COPY pkg/golang*.deb /opt/toolcache/
COPY pkg/java-8*.deb /opt/toolcache/
COPY pkg/java-11*.deb /opt/toolcache/
COPY pkg/license*.deb /opt/toolcache/
COPY pkg/mono*.deb /opt/toolcache/
COPY pkg/node*.deb /opt/toolcache/
COPY pkg/php*.deb /opt/toolcache/
COPY pkg/python*.deb /opt/toolcache/
COPY pkg/ruby*.deb /opt/toolcache/
COPY pkg/rust*.deb /opt/toolcache/
COPY config/install.sh /opt/install.sh
RUN bash /opt/install.sh
COPY run.sh /
ENTRYPOINT ["/run.sh"]
