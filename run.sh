#!/bin/bash -l

set -e
# shellcheck disable=SC1091
. /opt/gitlab/.bashrc || true

[[ -z ${SETUP_CMD:-} ]] && set -uo pipefail

export CI_API_V4_URL="${CI_API_V4_URL:-https://gitlab.com/api/v4}"
export DEBIAN_FRONTEND=noninteractive
export DOTNET_CLI_TELEMETRY_OPTOUT=1
export GO111MODULE=on
export GOPATH=/opt/gitlab/.local
export HISTFILESIZE=0
export HISTSIZE=0
export LANG=C.UTF-8
export LICENSE_FINDER_CLI_OPTS=${LICENSE_FINDER_CLI_OPTS:=--no-debug}
export LM_JAVA_VERSION=${LM_JAVA_VERSION:-"8"}
export LM_PYTHON_VERSION=${LM_PYTHON_VERSION:-"3"}
export LM_REPORT_FILE=${LM_REPORT_FILE:-'gl-license-scanning-report.json'}
export MAVEN_CLI_OPTS="${MAVEN_CLI_OPTS:--DskipTests}"
export NO_UPDATE_NOTIFIER=true
export PIPENV_VENV_IN_PROJECT=1
export PREPARE="${PREPARE:---prepare-no-fail}"
export RECURSIVE='--no-recursive'
export RUBY_GC_HEAP_INIT_SLOTS=800000
export RUBY_GC_MALLOC_LIMIT=79000000
export RUBY_HEAP_FREE_MIN=100000
export RUBY_HEAP_SLOTS_GROWTH_FACTOR=1
export RUBY_HEAP_SLOTS_INCREMENT=400000

project_dir="${CI_PROJECT_DIR:-${@: -1}}"
[[ -d $project_dir ]] && cd "$project_dir"

function scan_project() {
  echo license_management report "$@"
  # shellcheck disable=SC2068
  license_management report $@
}

function prepare_project() {
  if [[ -n ${SETUP_CMD:-} ]]; then
    echo "Running '${SETUP_CMD}' to install project dependencies…"
    # shellcheck disable=SC2068
    ${SETUP_CMD[@]}
    PREPARE="--no-prepare"
  fi
  license_management ignored_groups add development
  license_management ignored_groups add develop
  license_management ignored_groups add test
}

prepare_project

scan_project "$PREPARE" \
  --format=json \
  --save="${LM_REPORT_FILE}" \
  "$RECURSIVE" \
  "$LICENSE_FINDER_CLI_OPTS"
