#!/bin/bash -l

set -ex

dpkg -i /opt/toolcache/php*.deb
asdf reshim
composer install --no-cache --no-scripts --dev --verbose --ignore-platform-reqs
