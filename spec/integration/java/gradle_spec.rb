# frozen_string_literal: true

require 'spec_helper'

RSpec.describe "gradle" do
  subject { runner.scan(env: env) }

  let(:env) { {} }

  include_examples "each report version", "java", "gradle", "dca34b80"

  context "when running a default gradle build" do
    before do
      runner.add_file('build.gradle') do
        <<~GRADLE
plugins {
  id "com.github.hierynomus.license" version "0.15.0"
}
        GRADLE
      end
    end

    it 'scans a gradle project' do
      expect(subject).to match_schema
      expect(subject[:licenses]).to be_empty
      expect(subject[:dependencies]).to be_empty
    end
  end

  context 'when scanning a gradle project that does not include the `com.github.hierynomus.license` plugin' do
    let(:project_url) { 'https://gitlab.com/one-touch-pipeline/otp.git' }

    before do
      runner.clone(project_url)
    end

    it 'is able to detect licenses' do
      expect(subject).to match_schema
      expect(subject[:licenses]).not_to be_empty

      [
        { name: 'ant', licenses: ['Apache-2.0'] },
        { name: 'activation', licenses: ['CDDL-1.0'] },
        { name: 'xml-apis', licenses: ['Apache-2.0', 'SAX-PD', 'W3C-20150513'] },
        { name: 'sitemesh', licenses: ['Apache-1.1'] },
        { name: 'hibernate-jpa-2.1-api', licenses: ['BSD-3-Clause', 'EPL-1.0'] }
      ].each do |dependency|
        expect(subject.licenses_for(dependency[:name])).to match_array(dependency[:licenses])
      end
    end
  end

  context 'when scanning a project that needs to connect to multiple TLS endpoints with different custom certificate chains' do
    let(:env) do
      {
        'ADDITIONAL_CA_CERT_BUNDLE' => x509_certificate.read,
        'PRIVATE_MAVEN_HOST' => 'maven.test'
      }
    end

    before do
      runner.mount(dir: fixture_file('java/gradle/offline-environment'))
    end

    specify do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array([
        "antlr",
        "commons-beanutils",
        "commons-io",
        "commons-lang",
        "commons-lang3",
        "fastutil",
        "findbugs-annotations",
        "geode-common",
        "geode-core",
        "geode-json",
        "jackson-annotations",
        "jackson-core",
        "jackson-databind",
        "javax.resource-api",
        "javax.transaction-api",
        "jgroups",
        "jna",
        "jopt-simple",
        "log4j-api",
        "log4j-core",
        "maven-artifact",
        "netty",
        "plexus-utils",
        "rhino",
        "shiro-core",
        "slf4j-api"
      ])
    end
  end

  context "when scanning a gradle project with a custom option to generate a profiler report" do
    let(:env) { { 'GRADLE_CLI_OPTS' => '--profile' } }

    before do
      runner.mount(dir: fixture_file("java/gradle/java-11"))
    end

    specify { expect { subject }.to change { Dir.glob("#{runner.project_path}/build/reports/profile/profile-*.html").count }.from(0).to(1) }

    specify do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array(['postgresql'])
      expect(subject.licenses_for('postgresql')).to match_array(['BSD-2-Clause'])
    end
  end

  context 'when using Java 8 with version 1.* of gradle' do
    let(:env) { { 'LM_JAVA_VERSION' => '8' } }

    before do
      runner.mount(dir: fixture_file("java/gradle/java-8"))
      runner.add_file('.tool-versions', "gradle 1.9")
    end

    specify do
      expect(subject).to match_schema
      expect(subject.dependency_names).to match_array(['postgresql'])
      expect(subject.licenses_for('postgresql')).to match_array(['BSD-2-Clause'])
    end
  end

  ['4.9', '5.6', '6.3'].each do |gradle_version|
    context "when using Java v11 with a kotlin project using gradle v#{gradle_version}" do
      let(:env) do
        {
          'LM_JAVA_VERSION' => '11',
          'GRADLE_CLI_OPTS' => '-b build.gradle.kts'
        }
      end

      before do
        runner.add_file('.tool-versions', "gradle #{gradle_version}")
        runner.add_file('build.gradle.kts', fixture_file_content("java/gradle/build.gradle.kts"))
        runner.add_file('settings.gradle.kts', 'rootProject.name = "example"')
      end

      specify do
        expect(subject).to match_schema
        expect(subject.dependency_names).to match_array(['postgresql'])
        expect(subject.licenses_for('postgresql')).to match_array(['BSD-2-Clause'])
      end
    end
  end

  [
    { java: '8', gradle: ['2.14', '3.5'] },
    { java: '11', gradle: ['4.9', '5.6', '6.3'] }
  ].each do |item|
    item[:gradle].each do |gradle_version|
      context "when using Java v#{item[:java]} with a gradle v#{gradle_version} on a groovy project" do
        let(:env) { { 'LM_JAVA_VERSION' => item[:java] } }

        before do
          runner.add_file('.tool-versions', "gradle #{gradle_version}")
          runner.add_file('build.gradle', fixture_file_content("java/gradle/java-#{item[:java]}/build.gradle"))
          runner.add_file('settings.gradle', 'rootProject.name = "example"')
        end

        specify do
          expect(subject).to match_schema
          expect(subject.dependency_names).to match_array(['postgresql'])
          expect(subject.licenses_for('postgresql')).to match_array(['BSD-2-Clause'])
        end
      end
    end
  end

  context "when scanning gradle project in a sub directory" do
    let(:env) do
      {
        'LICENSE_FINDER_CLI_OPTS' => '--project-path=service/gradle-backend'
      }
    end

    before do
      runner.mount(dir: fixture_file('java/gradle/nested'))
    end

    specify { expect(subject).to match_schema }
    specify { expect(subject.dependency_names).to match_array(["postgresql"]) }
  end
end
